// Actions is a place to keep functions to run once duplicates are found.

package main

import (
	"fmt"
	"os"
	"sync"
)

// ListDupes will print out a list of duplicates.
func ListDupes(mappedChecksumMatches map[FileDescription][]FileDescription) {
	fmt.Printf("\nDuplicate files:\n")
	for refFileDesc, searchFileDescs := range mappedChecksumMatches {
		fmt.Printf("%s:\n", refFileDesc.FileInfo.Name())
		for _, searchFileDesc := range searchFileDescs {
			fmt.Printf("\t%s\n", searchFileDesc.FileInfo.Name())
		}
		fmt.Printf("\n")
	}
}

// DeleteDupes is used to delete all of the dupes to be found in the search directory.
func DeleteDupes(mappedChecksumMatches map[FileDescription][]FileDescription) {
	fmt.Printf("\nDeleting duplicate files:\n")
	wg := &sync.WaitGroup{}
	// Iterate over each ref file with dupes in the outer loop.
	for refFileDesc, searchFileDescs := range mappedChecksumMatches {
		// In the inner loop, iterate over each duplicate of the ref file to have been found in the search dir(s).
		for _, searchFileDesc := range searchFileDescs {
			wg.Add(1)
			// Spin up a goroutine for each file to delete.
			go func(refFileDesc FileDescription, searchFileDesc FileDescription) {
				os.Remove(searchFileDesc.AbsPath) // Delete the file.
				fmt.Printf("\tDeleted %s (Dupe of %s)\n", searchFileDesc.AbsPath, refFileDesc.AbsPath)
				wg.Done()
			}(refFileDesc, searchFileDesc)
		}
	}
	// Wait for all files to be deleted.
	wg.Wait()
	fmt.Printf("\nAll duplicate files have been removed.\n")
}
