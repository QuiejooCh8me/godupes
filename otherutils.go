// This is for other functions that don't belong elsewhere.

package main

// StringInSlice returns true if the string is in the slice.
// It returns false if the string is not in the slice.
func StringInSlice(str string, slc []string) bool {
	for _, b := range slc {
		if b == str {
			return true
		}
	}
	return false
}

// FileDescInSlice returns true if the FileDescription is in the slice.
// It returns false if the FileDescription is not in the slice.
func FileDescInSlice(a FileDescription, slc []FileDescription) bool {
	for _, b := range slc {
		if b == a {
			return true
		}
	}
	return false
}
