// Tests for functions in actions.go

package main

import (
	"os"
	"testing"
)

// This is meant to actually test the os methods, do not mock those.
func TestDeleteDupes(t *testing.T) {
	// Open these files to create them.
	testa, err := os.OpenFile("/tmp/testa", os.O_RDONLY|os.O_CREATE, 0666)
	checkError(err)
	testb, err := os.OpenFile("/tmp/testb", os.O_RDONLY|os.O_CREATE, 0666)
	checkError(err)
	testa.Close()
	testb.Close()

	// Check that the files exist before the test.
	aExists, err := PathExists("/tmp/testa")
	checkError(err)
	bExists, err := PathExists("/tmp/testb")
	checkError(err)
	if !aExists || !bExists {
		t.Error("Could not create test files.")
	}

	testMap := make(map[FileDescription][]FileDescription)
	testRef := FileDescription{AbsPath: "UNIT TEST"}
	testMap[testRef] = make([]FileDescription, 2)
	testMap[testRef][0] = FileDescription{AbsPath: "/tmp/testa"}
	testMap[testRef][1] = FileDescription{AbsPath: "/tmp/testb"}

	DeleteDupes(testMap)

	// Check that the files exist before the test.
	aExists, err = PathExists("/tmp/testa")
	checkError(err)
	bExists, err = PathExists("/tmp/testb")
	checkError(err)
	if aExists || bExists {
		t.Error("Could not delete duplicate files.")
	}
}
