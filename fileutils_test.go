// Contains tests for functions in fileutils.go.
// All of these tests should use real files/directories if possible.

package main

import (
	"io/ioutil"
	"os"
	"testing"
)

func buildTestDirsWithFiles() {
	// Some paths to make things handy.
	// dirPathA := "/tmp/godupes-testa"
	filePathA := "/tmp/godupes-testa/test_file_a"
	// dirPathB := "/tmp/godupes-testa/testb"
	filePathB := "/tmp/godupes-testa/testb/test_file_b"
	dirPathC := "/tmp/godupes-testa/testb/testc"
	filePathC := "/tmp/godupes-testa/testb/testc/test_file_c"
	dirPathD := "/tmp/godupes-testa/testb/testd"
	filePathD := "/tmp/godupes-testa/testb/testd/test_file_d"

	// Clean things up before testing.
	removeTestDirsWithFiles()

	// Build a test dir tree.
	err := os.MkdirAll(dirPathC, 0777)
	checkError(err)
	err = os.Mkdir(dirPathD, 0777)
	checkError(err)

	// Add files to the dir tree.
	err = ioutil.WriteFile(filePathA, []byte("testa"), 0774)
	checkError(err)
	err = ioutil.WriteFile(filePathB, []byte("testb"), 0774)
	checkError(err)
	err = ioutil.WriteFile(filePathC, []byte("testc"), 0774)
	checkError(err)
	err = ioutil.WriteFile(filePathD, []byte("testd"), 0774)
	checkError(err)
}

func removeTestDirsWithFiles() {
	err := os.RemoveAll("/tmp/godupes-testa")
	checkError(err)
}

func TestPathExists(t *testing.T) {
	homeExists, err := PathExists("/home")
	checkError(err)
	if !homeExists {
		t.Errorf("PathExists does not think /home exists.")
	}
	garbageExists, err := PathExists("/fsjakldfkjeiorjw")
	checkError(err)
	if garbageExists {
		t.Errorf("PathExists says a non-existent path exists.")
	}
}

func TestAbsolutePath(t *testing.T) {
	// Clean up before the file test.
	filePath := "/tmp/godupes-testa"
	err := os.RemoveAll(filePath)
	checkError(err)
	err = ioutil.WriteFile(filePath, []byte("test"), 0774)
	checkError(err)
	fileAbsPath := AbsolutePath(filePath, true)
	if fileAbsPath != filePath {
		t.Errorf("AbsolutePath did not return the absolute path of a file.")
	}

	// Clean up before the dir test.
	err = os.RemoveAll(filePath)
	checkError(err)
	err = os.Mkdir(filePath, 0777)
	checkError(err)
	dirAbsPath := AbsolutePath(filePath, true)
	if dirAbsPath == filePath {
		t.Errorf("AbsolutePath did not add a trailing slash to the absolute path of a directory.")
	}
	if dirAbsPath != "/tmp/godupes-testa/" {
		t.Errorf("AbsolutePath did not return the absolute path of a directory.")
	}
	dirAbsPath = AbsolutePath(filePath, false)
	if dirAbsPath != filePath {
		t.Errorf("AbsolutePath did not return the absolute path of a directory.")
	}
	// Clean up when done.
	err = os.RemoveAll(filePath)
	checkError(err)
}

func TestBuildDirsSlice(t *testing.T) {
	// Some paths to make things handy.
	dirPathA := "/tmp/godupes-testa"
	// filePathA := "/tmp/godupes-testa/test_file_a"
	dirPathB := "/tmp/godupes-testa/testb"
	// filePathB := "/tmp/godupes-testa/testb/test_file_b"
	dirPathC := "/tmp/godupes-testa/testb/testc"
	// filePathC := "/tmp/godupes-testa/testb/testc/test_file_c"
	dirPathD := "/tmp/godupes-testa/testb/testd"
	// filePathD := "/tmp/godupes-testa/testb/testd/test_file_d"

	buildTestDirsWithFiles()

	// Test without recursion.
	useRecursion := false
	flatDirsSlice := BuildDirsSlice(&dirPathA, &useRecursion)
	if len(flatDirsSlice.S) != 1 {
		t.Errorf("BuildDirsSlice failed to correctly build a slice of directories without using recursion (wrong number of dirs in slice).\n\tWanted: 1\n\tGot: %d\n\t%#v", len(flatDirsSlice.S), flatDirsSlice.S)
	}
	for _, dir := range flatDirsSlice.S {
		if dir != "/tmp/godupes-testa" {
			t.Errorf("BuildDirsSlice failed to correctly build a slice of directories without using recursion (wrong dir name in slice).\n\t%s", dir)
		}
	}

	// Test with recursion.
	useRecursion = true
	dirsSlice := BuildDirsSlice(&dirPathA, &useRecursion)
	if len(dirsSlice.S) != 4 {
		t.Errorf("BuildDirsSlice failed to correctly build a slice of directories using recursion (wrong number of dirs in slice).\n\tWanted: 4\n\tGot: %d\n\t%#v", len(flatDirsSlice.S), flatDirsSlice.S)
	}
	for _, dir := range dirsSlice.S {
		if dir != dirPathA &&
			dir != dirPathB &&
			dir != dirPathC &&
			dir != dirPathD {
			t.Errorf("BuildDirsSlice failed to correctly build a slice of directories using recursion (wrong dir name in slice).\n\t%s", dir)
		}
	}

	// Clean things up after testing.
	removeTestDirsWithFiles()
}

func TestScanDirForFileInfo(t *testing.T) {
	// Clean up before testing
	removeTestDirsWithFiles()

	// Build a test dir tree with files.
	buildTestDirsWithFiles()
	// Some paths to make things handy.
	dirPathA := "/tmp/godupes-testa"
	filePathA := "/tmp/godupes-testa/test_file_a"
	dirPathB := "/tmp/godupes-testa/testb"
	filePathB := "/tmp/godupes-testa/testb/test_file_b"
	dirPathC := "/tmp/godupes-testa/testb/testc"
	filePathC := "/tmp/godupes-testa/testb/testc/test_file_c"
	dirPathD := "/tmp/godupes-testa/testb/testd"
	filePathD := "/tmp/godupes-testa/testb/testd/test_file_d"

	// Set up a slice of dir paths.
	dirs := make([]string, 0)
	dirs = append(dirs, dirPathA)
	dirs = append(dirs, dirPathB)
	dirs = append(dirs, dirPathC)
	dirs = append(dirs, dirPathD)

	// Run the function to test.
	fileDescSlice := ScanDirForFileInfo(dirs)

	// Test the results.
	testaOK := false
	testbOK := false
	testcOK := false
	testdOK := false

	if len(fileDescSlice.S) != 4 {
		t.Errorf("ScanDirForFileInfo returned the wrong number of FileDescriptions.\n\tWanted: 4\n\tGot: %d", len(fileDescSlice.S))
	}

	for _, fileDesc := range fileDescSlice.S {
		filePath := fileDesc.AbsPath
		switch filePath {
		case filePathA:
			if fileDesc.FileInfo.Name() == "test_file_a" {
				testaOK = true
			}
		case filePathB:
			if fileDesc.FileInfo.Name() == "test_file_b" {
				testbOK = true
			}
		case filePathC:
			if fileDesc.FileInfo.Name() == "test_file_c" {
				testcOK = true
			}
		case filePathD:
			if fileDesc.FileInfo.Name() == "test_file_d" {
				testdOK = true
			}
		}
		if fileDesc.AbsPath == filePathA {
			if fileDesc.FileInfo.Name() == "test_file_a" {
				testaOK = true
			}
		}
		if fileDesc.AbsPath == filePathB {
			if fileDesc.FileInfo.Name() == "test_file_b" {
				testbOK = true
			}
		}
		if fileDesc.AbsPath == filePathC {
			if fileDesc.FileInfo.Name() == "test_file_c" {
				testcOK = true
			}
		}
		if fileDesc.AbsPath == filePathD {
			if fileDesc.FileInfo.Name() == "test_file_d" {
				testdOK = true
			}
		}
	}

	if !testaOK ||
		!testbOK ||
		!testcOK ||
		!testdOK {
		t.Errorf("ScanDirForFileInfo at least one FileDescription was missing/incorrect.")
	}

	// Clean up after testing
	removeTestDirsWithFiles()
}

func TestFilesToChecksum(t *testing.T) {
	// First create a []PairedFiles
	fileDescMatches := make([]PairedFiles, 0)
	fileDescMatches = append(fileDescMatches, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testa"}, SearchFileDesc: FileDescription{AbsPath: "testb"}})
	fileDescMatches = append(fileDescMatches, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testc"}, SearchFileDesc: FileDescription{AbsPath: "testd"}})
	fileDescMatches = append(fileDescMatches, PairedFiles{RefFileDesc: FileDescription{AbsPath: "teste"}, SearchFileDesc: FileDescription{AbsPath: "testf"}})
	fileDescMatches = append(fileDescMatches, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testa"}, SearchFileDesc: FileDescription{AbsPath: "testd"}})
	fileDescMatches = append(fileDescMatches, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testb"}, SearchFileDesc: FileDescription{AbsPath: "testf"}})

	filesToChecksum := FilesToChecksum(&fileDescMatches)

	testaOK := 0
	testbOK := 0
	testcOK := 0
	testdOK := 0
	testeOK := 0
	testfOK := 0

	if len(*filesToChecksum) != 6 {
		t.Errorf("FilesToChecksum returned the wrong number of file paths.\n\tWanted: 6\n\tGot: %d", len(*filesToChecksum))
	}

	for _, filePath := range *filesToChecksum {
		switch filePath {
		case "testa":
			testaOK++
		case "testb":
			testbOK++
		case "testc":
			testcOK++
		case "testd":
			testdOK++
		case "teste":
			testeOK++
		case "testf":
			testfOK++
		}
	}

	if testaOK != 1 || testbOK != 1 || testcOK != 1 ||
		testdOK != 1 || testeOK != 1 || testfOK != 1 {
		t.Errorf("FilesToChecksum returned incorrect file paths.")
	}

}

func TestChecksumFiles(t *testing.T) {
	// Clean up before test
	removeTestDirsWithFiles()

	// build a test dir tree
	buildTestDirsWithFiles()

	// Some paths to make things handy.
	// dirPathA := "/tmp/godupes-testa"
	filePathA := "/tmp/godupes-testa/test_file_a"
	// dirPathB := "/tmp/godupes-testa/testb"
	filePathB := "/tmp/godupes-testa/testb/test_file_b"
	// dirPathC := "/tmp/godupes-testa/testb/testc"
	filePathC := "/tmp/godupes-testa/testb/testc/test_file_c"
	// dirPathD := "/tmp/godupes-testa/testb/testd"
	filePathD := "/tmp/godupes-testa/testb/testd/test_file_d"

	// Create the []string of file paths
	filesToChecksum := make([]string, 0)
	filesToChecksum = append(filesToChecksum, filePathA)
	filesToChecksum = append(filesToChecksum, filePathB)
	filesToChecksum = append(filesToChecksum, filePathC)
	filesToChecksum = append(filesToChecksum, filePathD)
	filesToChecksum = append(filesToChecksum, filePathD) // add twice to check that things dedupe.

	fileChecksums := ChecksumFiles(&filesToChecksum)

	testaOK := true

	if len(fileChecksums) != 4 {
		t.Errorf("ChecksumFiles returned the wrong number of file checksums.\n\tWanted: 4\n\tGot: %d", len(fileChecksums))
	}

	for filePath, fileHash := range fileChecksums {
		switch filePath {
		case "/tmp/godupes-testa/testb/testd/test_file_d":
			if fileHash != "a4853613b2a38568ed4e49196238152469097412d06d5e5fc9be8ab92cfdf2bf" {
				testaOK = false
			}
		case "/tmp/godupes-testa/testb/test_file_b":
			if fileHash != "8f7f42774a73d9ca58b6b78921d1df4d6b9dc780e46e5180ebf36426691b7bcd" {
				testaOK = false
			}
		case "/tmp/godupes-testa/testb/testc/test_file_c":
			if fileHash != "b200375c9b60737f76cf19cadc801b4351ba3ccd70a45c97c6efb7de2d8dda57" {
				testaOK = false
			}
		case "/tmp/godupes-testa/test_file_a":
			if fileHash != "4417a30dc8c6b53f5e2e2b9051159348036017f9061e8ace1f966a1ab58fbedd" {
				testaOK = false
			}
		}
	}

	if !testaOK {
		t.Errorf("ChecksumFiles returned one or more incorrect file checksums.")
	}

	// Clean up after test
	removeTestDirsWithFiles()
}

func TestAddChecksumsToMatched(t *testing.T) {
	// Set up a mock fileChecksumMatches
	fileChecksums := map[string]string{"/tmp/godupes-testa/testb/testd/test_file_d": "a4853613b2a38568ed4e49196238152469097412d06d5e5fc9be8ab92cfdf2bf",
		"/tmp/godupes-testa/testb/testc/test_file_c": "b200375c9b60737f76cf19cadc801b4351ba3ccd70a45c97c6efb7de2d8dda57",
		"/tmp/godupes-testa/testb/test_file_b":       "8f7f42774a73d9ca58b6b78921d1df4d6b9dc780e46e5180ebf36426691b7bcd",
		"/tmp/godupes-testa/test_file_a":             "4417a30dc8c6b53f5e2e2b9051159348036017f9061e8ace1f966a1ab58fbedd"}

	// set up a mock fileDescMatches
	fileDescMatches := []PairedFiles{PairedFiles{RefFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/testb/testd/test_file_d"}, SearchFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/testb/testc/test_file_c"}},
		PairedFiles{RefFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/testb/test_file_b"}, SearchFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/test_file_a"}}}

	checksumMatches := AddChecksumsToMatched(fileChecksums, &fileDescMatches)

	// save a correct result to compare to.
	refPairs := []PairedFiles{PairedFiles{RefFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/testb/testd/test_file_d",
		FileInfo: os.FileInfo(nil), Checksum: "a4853613b2a38568ed4e49196238152469097412d06d5e5fc9be8ab92cfdf2bf"}, SearchFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/testb/testc/test_file_c",
		FileInfo: os.FileInfo(nil), Checksum: "b200375c9b60737f76cf19cadc801b4351ba3ccd70a45c97c6efb7de2d8dda57"}}, PairedFiles{RefFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/testb/test_file_b",
		FileInfo: os.FileInfo(nil), Checksum: "8f7f42774a73d9ca58b6b78921d1df4d6b9dc780e46e5180ebf36426691b7bcd"}, SearchFileDesc: FileDescription{AbsPath: "/tmp/godupes-testa/test_file_a",
		FileInfo: os.FileInfo(nil), Checksum: "4417a30dc8c6b53f5e2e2b9051159348036017f9061e8ace1f966a1ab58fbedd"}}}

	if len(*checksumMatches) != 2 {
		t.Errorf("AddChecksumsToMatched returned the wrong number of file checksums.\n\tWanted: 2\n\tGot: %d", len(*checksumMatches))
	}

	testaOK := 0

	for _, testPair := range *checksumMatches {
		for _, refPair := range refPairs {
			if refPair == testPair {
				testaOK++
			}
		}
	}

	if testaOK != 2 {
		t.Errorf("AddChecksumsToMatched did not return the correct PairedFiles.")
	}
}
