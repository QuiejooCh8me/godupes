// errorutils contains error handling code.
package main

import "log"

// checkError checks for an error and handles it appropriately.
func checkError(err error) {
	if err != nil {
		// Use a switch here to handle different error types if we need to.
		switch err.(type) {
		default:
			log.Fatal(err)
		}
	}
}
