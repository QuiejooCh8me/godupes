// matching_test contains tests for the functions in matching.go

package main

import (
	"io/ioutil"
	"os"
	"testing"
)

// TODO: Add checking of comparators other than UseSize.
func TestFindMatchingFileDescriptions(t *testing.T) {
	// First create some test file contents.
	testContents := make([][]byte, 0)
	testContents = append(testContents, []byte("This is a test."))
	testContents = append(testContents, []byte("This is another test."))
	testContents = append(testContents, []byte("This should not show up."))
	testContents = append(testContents, []byte("This should not show up, either."))

	// Clean up before the test.
	err := os.RemoveAll("/tmp/godupes-testa")
	checkError(err)
	err = os.RemoveAll("/tmp/godupes-testb")
	checkError(err)

	// Create the test directories.
	os.Mkdir("/tmp/godupes-testa", 0777)
	os.Mkdir("/tmp/godupes-testb", 0777)

	// Create the test ref files.
	refFilePaths := make([]string, 0)
	refFilePaths = append(refFilePaths, "/tmp/godupes-testa/testa1")
	refFilePaths = append(refFilePaths, "/tmp/godupes-testa/testa2")
	refFilePaths = append(refFilePaths, "/tmp/godupes-testa/testa3")
	for i, filePath := range refFilePaths {
		idx := i
		if i == 2 {
			// Make the last files not match.
			idx = 3
		}
		err = ioutil.WriteFile(filePath, testContents[idx], 0774)
		checkError(err)
	}
	// Create the test search files.
	searchFilePaths := make([]string, 0)
	searchFilePaths = append(searchFilePaths, "/tmp/godupes-testb/testb1")
	searchFilePaths = append(searchFilePaths, "/tmp/godupes-testb/testb2")
	searchFilePaths = append(searchFilePaths, "/tmp/godupes-testb/testb3")
	for i, filePath := range searchFilePaths {
		err = ioutil.WriteFile(filePath, testContents[i], 0644)
		checkError(err)
	}

	// Create the FileDescriptions.
	refFileDescs := make([]FileDescription, 0)
	searchFileDescs := make([]FileDescription, 0)

	for _, filePath := range refFilePaths {
		fileInfo, err := os.Stat(filePath)
		checkError(err)
		refFileDescs = append(refFileDescs, FileDescription{AbsPath: filePath, FileInfo: fileInfo})
	}
	for _, filePath := range searchFilePaths {
		fileInfo, err := os.Stat(filePath)
		checkError(err)
		searchFileDescs = append(searchFileDescs, FileDescription{AbsPath: filePath, FileInfo: fileInfo})
	}

	// Test the UseSize path.
	fileDescMatches := FindMatchingFileDescriptions(&refFileDescs, &searchFileDescs, CompareFields{UseName: false, UseSize: true, UseModDate: false, UseChecksum: false})

	testaOK := false // Check that matches work
	testbOK := false // Check that matches work
	testcOK := true  // Check that non-matches aren't matched
	testdOK := true  // Check that non-matches aren't matched

	for _, fileDescMatch := range *fileDescMatches {
		if fileDescMatch.RefFileDesc.FileInfo.Name() == "testa1" &&
			fileDescMatch.SearchFileDesc.FileInfo.Name() == "testb1" {
			testaOK = true
		}
		if fileDescMatch.RefFileDesc.FileInfo.Name() == "testa2" &&
			fileDescMatch.SearchFileDesc.FileInfo.Name() == "testb2" {
			testbOK = true
		}
		if fileDescMatch.RefFileDesc.FileInfo.Name() == "testa3" ||
			fileDescMatch.SearchFileDesc.FileInfo.Name() == "testb3" {
			testcOK = false
		}
		if fileDescMatch.RefFileDesc.FileInfo.Name() == "testa1" &&
			fileDescMatch.SearchFileDesc.FileInfo.Name() == "testb2" {
			testdOK = false
		}
	}

	if !testaOK || !testbOK {
		t.Errorf("FindMatchingFileDescriptions did not match files by size as it should have. (testa: %t, testb: %t)", testaOK, testbOK)
	}
	if !testcOK || !testdOK {
		t.Errorf("FindMatchingFileDescriptions matched files by size that should not have been matched. (testc: %t, testd: %t)", testcOK, testdOK)
	}

	// Clean up after the test.
	err = os.RemoveAll("/tmp/godupes-testa")
	checkError(err)
	err = os.RemoveAll("/tmp/godupes-testb")
	checkError(err)
}

func TestFindMatchingFileChecksums(t *testing.T) {
	// Create some test matches with checksums that match.
	matchingChecksums := make([]PairedFiles, 0)
	matchingChecksums = append(matchingChecksums, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testa", Checksum: "i>8Q!MO@$/tLFM!RzLu<"}, SearchFileDesc: FileDescription{AbsPath: "testaa", Checksum: "i>8Q!MO@$/tLFM!RzLu<"}})

	matchingChecksums = append(matchingChecksums, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testb", Checksum: "WBS2+k.9YX30>e'g}6G%"}, SearchFileDesc: FileDescription{AbsPath: "testbb", Checksum: "WBS2+k.9YX30>e'g}6G%"}})

	// create some test matches checksums that don't match.
	badChecksums := make([]PairedFiles, 0)
	badChecksums = append(badChecksums, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testc", Checksum: "Q~2GPmnJ_h1/?A=uhV{6"}, SearchFileDesc: FileDescription{AbsPath: "testcc", Checksum: "4,OGbz0e,su(AUF*|zS0"}})

	badChecksums = append(badChecksums, PairedFiles{RefFileDesc: FileDescription{AbsPath: "testd", Checksum: "5)+]o_b{W<DHFfEXQ}rk"}, SearchFileDesc: FileDescription{AbsPath: "testdd", Checksum: "_%Vy$zJcBI5B[C}qAO0w"}})

	combinedChecksums := make([]PairedFiles, 0)
	combinedChecksums = append(combinedChecksums, matchingChecksums...)
	combinedChecksums = append(combinedChecksums, badChecksums...)

	fileChecksumMatches := FindMatchingFileChecksums(&combinedChecksums)

	nilFileDescription := FileDescription{}
	testaOK := false
	testbOK := false
	testcOK := true
	testdOK := true

	for _, checksumMatch := range *fileChecksumMatches {
		// Check to make sure no nil FileDescriptions make it back.
		if checksumMatch.RefFileDesc == nilFileDescription ||
			checksumMatch.SearchFileDesc == nilFileDescription {
			t.Errorf("FindMatchingFileChecksums returned a slice with PairedFiles containing one or more nil FileDescriptions.")
		}
		if checksumMatch.RefFileDesc.AbsPath == "testa" &&
			checksumMatch.SearchFileDesc.AbsPath == "testaa" {
			testaOK = true
		}
		if checksumMatch.RefFileDesc.AbsPath == "testb" &&
			checksumMatch.SearchFileDesc.AbsPath == "testbb" {
			testbOK = true
		}
		if checksumMatch.RefFileDesc.AbsPath == "testc" ||
			checksumMatch.SearchFileDesc.AbsPath == "testcc" {
			testcOK = false
		}
		if checksumMatch.RefFileDesc.AbsPath == "testd" ||
			checksumMatch.SearchFileDesc.AbsPath == "testdd" {
			testdOK = false
		}
	}

	if !testaOK || !testbOK {
		t.Errorf("FindMatchingFileChecksums did not match two checksums that should have matched. (testaOK: %t, testbOK: %t)", testaOK, testbOK)
	}
	if !testcOK || !testdOK {
		t.Errorf("FindMatchingFileChecksums matched two checksums that should not have matched. (testcOK: %t, testdOK: %t)", testcOK, testdOK)
	}
}

// In TestMappedChecksumMatches, be sure to test:
//  - Empty checksums
//  - Matching checksums
//  - Non-matching checksums
//  - Matching AbsPaths
func TestMappedChecksumMatches(t *testing.T) {
	// Create some fake data.
	fileChecksumMatches := make([]PairedFiles, 8)

	fileChecksumMatches[0] = PairedFiles{RefFileDesc: FileDescription{AbsPath: "testa", Checksum: "thisisatest"}, SearchFileDesc: FileDescription{AbsPath: "testaa", Checksum: "thisisatest"}}

	fileChecksumMatches[1] = PairedFiles{RefFileDesc: FileDescription{AbsPath: "testa", Checksum: "thisisatest"}, SearchFileDesc: FileDescription{AbsPath: "testaaa", Checksum: "thisisatest"}}

	fileChecksumMatches[2] = PairedFiles{RefFileDesc: FileDescription{AbsPath: "testb", Checksum: "X>fEZMl?Syuh7.(nm1E%"}, SearchFileDesc: FileDescription{AbsPath: "testbb", Checksum: "X>fEZMl?Syuh7.(nm1E%"}}

	fileChecksumMatches[3] = PairedFiles{RefFileDesc: FileDescription{AbsPath: "testc", Checksum: "K6%{'M.D,6LuA,hdQ+1v"}, SearchFileDesc: FileDescription{AbsPath: "testcc", Checksum: "b0,M##]{Klm7!3qj`Qq|"}}

	fileChecksumMatches[4] = PairedFiles{RefFileDesc: FileDescription{AbsPath: "testd", Checksum: ""}, SearchFileDesc: FileDescription{AbsPath: "testdd", Checksum: "b0,M##]{Klm7!3qj`Qq|"}}

	fileChecksumMatches[5] = PairedFiles{RefFileDesc: FileDescription{AbsPath: "testf"}, SearchFileDesc: FileDescription{AbsPath: "testff", Checksum: "b0,M##]{Klm7!3qj`Qq|"}}

	fileChecksumMatches[6] = PairedFiles{RefFileDesc: FileDescription{AbsPath: "teste", Checksum: "eW2,-KBi-Wn#{QezPAUx"}, SearchFileDesc: FileDescription{AbsPath: "teste", Checksum: "eW2,-KBi-Wn#{QezPAUx"}}

	// Feed in the fake data and save the result.
	mappedChecksumMatches := MapChecksumMatches(&fileChecksumMatches)

	// Some flags to track
	testaOK := 0
	testbOK := false
	testcOK := true
	testdOK := true
	testeOK := true
	testfOK := true

	// Check the results.
	for refFileDesc, searchFileDescs := range mappedChecksumMatches.M {
		if refFileDesc.AbsPath == "testc" {
			testcOK = false
		}
		if refFileDesc.AbsPath == "testd" {
			testdOK = false
		}
		if refFileDesc.AbsPath == "teste" {
			testeOK = false
		}
		if refFileDesc.AbsPath == "testf" {
			testfOK = false
		}
		for _, searchFileDesc := range searchFileDescs {
			if refFileDesc.AbsPath == "testa" &&
				searchFileDesc.AbsPath == "testaa" {
				testaOK++
			}
			if refFileDesc.AbsPath == "testa" &&
				searchFileDesc.AbsPath == "testaaa" {
				testaOK++
			}
			if refFileDesc.AbsPath == "testb" &&
				searchFileDesc.AbsPath == "testbb" {
				testbOK = true
			}
		}
	}
	// Report any problems.
	if testaOK != 2 || !testbOK {
		t.Errorf("MapChecksumMatches failed to map two matching checksums. (testa: %d, testb: %t)", testaOK, testbOK)
	}
	if !testcOK {
		t.Errorf("MapChecksumMatches mapped two non-matching checksums. (testc: %t, testd: %t)", testcOK, testdOK)
	}
	if !testeOK {
		t.Errorf("MapChecksumMatches mapped two matching AbsPaths. (teste: %t)", testeOK)
	}
	if !testdOK || !testfOK {
		t.Errorf("MapChecksumMatches an empty/nil checksum. (testf: %t)", testfOK)
	}
}
