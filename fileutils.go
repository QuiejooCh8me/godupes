// fileutils contains functions for dealing with dirs/files.
package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/stretchr/powerwalk"
)

// FileHash is to pass around file checksums.
type FileHash struct {
	AbsPath  string
	Checksum string
}

// PathExists returns True when the given file or directory exists,
//  or False if it does not exist.
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// AbsolutePath returns the absolute path of path.
// Set trSep to true if you want a trailing slash.
func AbsolutePath(path string, trSep bool) string {
	var pathBuffer bytes.Buffer // Assemble the full path in this buffer.
	absPath, err := filepath.Abs(path)
	checkError(err)
	pathBuffer.WriteString(absPath) // Add the path to the file.

	// If the file is a dir, add a trailing separator.
	fileInfo, err := os.Stat(absPath)
	checkError(err)

	if fileInfo.IsDir() && trSep {
		pathBuffer.WriteString("/") // Add the trailing separator.
	}
	fileAbsPath := pathBuffer.String() // Put into string form.
	return fileAbsPath
}

// BuildDirsSlice builds a []string of dir path names within the globbed dirs.
func BuildDirsSlice(dirPtr *string, useRecursion *bool) *DirsSlice {
	// Find the reference directories.
	globResult, err := filepath.Glob(*dirPtr)
	checkError(err)
	dirs := DirsSlice{}
	// Need to find out how big to make the channel buffer (max number of files in all dirs).
	// If this is too small, execution will stall at the wg.Wait() below.
	var bufferLength struct {
		sync.Mutex
		i int
	}
	for _, dir := range globResult {
		err = powerwalk.Walk(dir, func(dir string, fileInfo os.FileInfo, err error) error {
			bufferLength.Lock()
			bufferLength.i++
			bufferLength.Unlock()
			return err
		})
	}
	resultsChan := make(chan string, bufferLength.i)
	var resultCount struct {
		sync.Mutex
		i int
	}
	wg := &sync.WaitGroup{}
	wg.Add(len(globResult))
	// Spin up a worker to find dirs in each glob result.
	for _, dir := range globResult {
		if *useRecursion {
			go func(path string) {
				// This path is for a recursive directory search.
				defer wg.Done()
				// filepath.Walk is incredibly slow and creates annoying race conditions, but powerwalk works.
				err = powerwalk.Walk(path, func(path string, fileInfo os.FileInfo, err error) error {
					if fileInfo.IsDir() {
						resultCount.Lock()
						resultCount.i++
						resultCount.Unlock()
						resultsChan <- path
					}
					return err
				})
			}(dir)
		} else {
			go func(path string) {
				// This path is for a non-recursive directory search.
				defer wg.Done()
				fileInfo, err := os.Stat(path)
				checkError(err)
				if fileInfo.IsDir() {
					resultCount.Lock()
					resultCount.i++
					resultCount.Unlock()
					resultsChan <- path
				}
			}(dir)
		}
	}
	// Wait for the buffer to fill up before processing.
	// If we don't then we won't know how many results to recieve.
	wg.Wait()
	// Results are waiting in the buffered channel now.
	// Capture each result from the channel.
	for i := 1; i <= resultCount.i; i++ {
		dirs.S = append(dirs.S, <-resultsChan)
	}
	return &dirs
}

// buildSingleFileDescription assembles a FileDescription for a file.
// This was an example to show how to concatenate strings using a bytes.Buffer (O(n) fast).
func buildSingleFileDescription(fileDescriptionsChannel chan FileDescription, fileInfo os.FileInfo, parentDirPath string) {
	var pathBuffer bytes.Buffer             // Assemble the full path in this buffer.
	pathBuffer.WriteString(parentDirPath)   // Add the path to the file.
	pathBuffer.WriteString("/")             // Add a separator
	pathBuffer.WriteString(fileInfo.Name()) // Add the basename of the file.
	fileAbsPath := pathBuffer.String()      // Put into string form.
	// Send the FileDescription through the channel.
	fileDescriptionsChannel <- FileDescription{AbsPath: fileAbsPath, FileInfo: fileInfo}
}

// BuildFileDescriptions spawns a goroutine for each file in filesInDir to assemble a FileDescription.
func buildFileDescriptions(fileDescriptionsChannel chan FileDescription, filesInDir *[]os.FileInfo, parentDirPath string) {
	for _, fileInfo := range *filesInDir {
		go func(fileDescriptionsChannel chan FileDescription, fileInfo os.FileInfo, parentDirPath string) {
			buildSingleFileDescription(fileDescriptionsChannel, fileInfo, parentDirPath)
		}(fileDescriptionsChannel, fileInfo, parentDirPath)
	}
}

// filterDirsOut removes dirs from a []os.FileInfo.
// This is used to make sure only FileInfos for files are left.
func filterDirsOut(filesInDir *[]os.FileInfo) *[]os.FileInfo {
	var filesOnlyInDir []os.FileInfo
	for _, fileInfo := range *filesInDir {
		if !fileInfo.IsDir() {
			filesOnlyInDir = append(filesOnlyInDir, fileInfo)
		}
	}
	return &filesOnlyInDir
}

// ScanDirForFileInfo iterates through filesInDir, skip directories (use IsDir() method), build a slice of FileDescriptions.
func ScanDirForFileInfo(dirs []string) FileDescSlice {
	dirsCount := len(dirs)

	fileScanJobsWG := &sync.WaitGroup{}
	fileScanJobsWG.Add(dirsCount)

	// Create the slice to stash results in.
	dirFileDescriptions := FileDescSlice{}

	for _, dir := range dirs {
		go func(dir string) {
			defer fileScanJobsWG.Done()
			filesInDir, err := ioutil.ReadDir(dir)
			checkError(err)
			// Make a new file info slice without directories.
			filesOnlyInDir := filterDirsOut(&filesInDir)
			filesCount := len(*filesOnlyInDir)
			var fileInfoWaitGroup sync.WaitGroup
			fileInfoWaitGroup.Add(filesCount)
			// Create a channel to transfer all file descriptions. (Potentially stashing a lot in memory here)
			fileDescriptionsChannel := make(chan FileDescription, filesCount)
			// Scan the directory to get info for all files in the channel.
			buildFileDescriptions(fileDescriptionsChannel, filesOnlyInDir, dir)
			// Recievers build the FileDescriptions slice as they come in on the channel.
			for i := 0; i <= filesCount-1; i++ {
				go func() {
					defer fileInfoWaitGroup.Done()
					dirFileDescriptions.Lock()
					dirFileDescriptions.S = append(dirFileDescriptions.S, <-fileDescriptionsChannel)
					dirFileDescriptions.Unlock()
				}()
			}
			fileInfoWaitGroup.Wait()
			close(fileDescriptionsChannel)
		}(dir)
	}
	fileScanJobsWG.Wait()
	return FileDescSlice{S: dirFileDescriptions.S}
}

// FilesToChecksum returns a slice of unique path names of files to checksum.
// This is necessary to "dedupe" the list of ref files and the list of search dirs.
// That way the SHA256sum for each file is only calculated once.
func FilesToChecksum(fileDescriptionMatches *[]PairedFiles) *[]string {
	var filesToChecksum []string
	// Iterate through fileDescriptionMatches.
	for _, matchedFiles := range *fileDescriptionMatches {
		refFile := matchedFiles.RefFileDesc.AbsPath
		searchFile := matchedFiles.SearchFileDesc.AbsPath
		if !StringInSlice(refFile, filesToChecksum) {
			// The refFile wasn't in the slice yet, add it here.
			filesToChecksum = append(filesToChecksum, refFile)
		}
		if !StringInSlice(searchFile, filesToChecksum) {
			// The searchFile wasn't in the slice yet, add it here.
			filesToChecksum = append(filesToChecksum, searchFile)
		}
	}
	return &filesToChecksum
}

// SHA256Checksum calculates then sends the FileHash for fileToChecksum back over fileChecksumChannel.
func sha256Checksum(fileChecksumChannel chan FileHash, fileToChecksum string) {
	file, err := os.Open(fileToChecksum) // open the fileToChecksum
	checkError(err)
	defer file.Close()                             // Close the file when done with it.
	hash := sha256.New()                           // Prepare an empty sha256 hash.
	if _, err := io.Copy(hash, file); err != nil { // Read the file into the hash function.
		log.Fatal(err)
	}
	// hash.Sum(nil) will return the checksum we're looking for.
	fileChecksumChannel <- FileHash{AbsPath: fileToChecksum, Checksum: hex.EncodeToString(hash.Sum(nil))}
}

// ChecksumFiles returns fileChecksums, a map[filePath]fileHash.
// fileChecksums is used to fill in the checksums for all files.
func ChecksumFiles(filesToChecksum *[]string) map[string]string {
	fileChecksums := make(map[string]string)
	fileChecksumChannel := make(chan FileHash, len(*filesToChecksum))
	fileChecksumWaitGroup := &sync.WaitGroup{}
	fileChecksumWaitGroup.Add(len(*filesToChecksum))
	for _, fileToChecksum := range *filesToChecksum {
		// Don't run a checksum on a file that's already done.
		if _, exists := fileChecksums[fileToChecksum]; exists {
			continue // Skip to the next file, this one is done already.
		} else {
			go func(fileChecksumChannel chan FileHash, fileToChecksum string) {
				defer fileChecksumWaitGroup.Done()
				sha256Checksum(fileChecksumChannel, fileToChecksum)
			}(fileChecksumChannel, fileToChecksum)
		}
	}
	fileChecksumWaitGroup.Wait()
	close(fileChecksumChannel)
	for fileHash := range fileChecksumChannel {
		fileChecksums[fileHash.AbsPath] = fileHash.Checksum
	}
	return fileChecksums
}

// AddChecksumsToMatched returns a []PairedFiles with the checksums filled in.
func AddChecksumsToMatched(fileChecksums map[string]string, fileDescMatches *[]PairedFiles) *[]PairedFiles {
	var checksumMatches []PairedFiles
	// Iterate through all matches from the stage 1 search.
	for _, matchedFiles := range *fileDescMatches {
		// Add the checksum to the refFileDesc.
		refFileDesc := matchedFiles.RefFileDesc
		refFileDesc.Checksum = fileChecksums[refFileDesc.AbsPath]
		// Add the checksum to the searchFileDesc.
		searchFileDesc := matchedFiles.SearchFileDesc
		searchFileDesc.Checksum = fileChecksums[searchFileDesc.AbsPath]
		checksumMatches = append(checksumMatches, PairedFiles{RefFileDesc: refFileDesc, SearchFileDesc: searchFileDesc})
	}
	return &checksumMatches
}
