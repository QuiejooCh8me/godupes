/*
Godupes is a CLI utility for finding and deleting files duplicated between directories.

The search directory is the directory to search for duplicate files to delete.
The reference directory is the directory to search for files to find duplicates of.

The duplicates are found in two stages:
  - The first uses cheap file properties. (fast)
  - The second uses SHA256 checksums. (slow)

Godupes is written to run on Linux/*BSD but could be made to work with any OS the Go compiler supports (mostly the tests would need to be modified).
*/
package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"sync"
)

// FileDescription is a struct to uniquely describe a file.
//   - Absolute Path
//   - os.FileInfo
//   - SHA256 Checksum
type FileDescription struct {
	AbsPath  string
	FileInfo os.FileInfo
	Checksum string
}

// DirsSlice is used as a slice with mutex lock for safety.
type DirsSlice struct {
	S []string
	sync.Mutex
}

// FileDescSlice is used as a slice with mutex lock for safety.
type FileDescSlice struct {
	S []FileDescription
	sync.Mutex
}

func main() {
	// Define the cmd line flags.
	searchDirectoryPtr := flag.String("search", "./", "The directory to search for files that match those in the reference directory (glob capable).")
	referenceDirectoryPtr := flag.String("ref", *searchDirectoryPtr, "The reference directory containing files you are searching for duplicates of. (default is the reference directory)")

	listDupesPtr := flag.Bool("l", true, "Godupes will print a list of duplicate files.")
	deleteDupesPtr := flag.Bool("delete", false, "Godupes will delete all duplicate files.")

	useRecursionPtr := flag.Bool("r", false, "Enables recursion into each ref/search directory.")

	useNamePtr := flag.Bool("n", false, "Enables 1st stage file name detection mode if set to true.")
	useSizePtr := flag.Bool("s", true, "Enables 1st stage file size detection mode if set to true.")
	useModDatePtr := flag.Bool("t", false, "Enables 1st stage modification time detection mode if set to true.")

	cpuCountPtr := flag.Int("p", runtime.NumCPU(), "Set the max number of CPUs to use.")
	runtime.GOMAXPROCS(*cpuCountPtr)

	debugModePtr := flag.Bool("dbg", false, "(optional) Enables debug messages if set to true.")

	// Capture/parse the cmd line flags.
	flag.Parse()

	fmt.Printf("Searching for directories to scan...")
	refDirs := BuildDirsSlice(referenceDirectoryPtr, useRecursionPtr)
	searchDirs := &DirsSlice{}
	if *searchDirectoryPtr != *referenceDirectoryPtr {
		searchDirs = BuildDirsSlice(searchDirectoryPtr, useRecursionPtr)
	} else {
		searchDirs.S = refDirs.S
	}
	fmt.Printf("\n\tFound %d reference directories + %d search directories.\n", len(refDirs.S), len(searchDirs.S))

	// Build 2 slices of FileDescriptions, one for each file in reference dir(s) + one for each file in search dir(s).
	fmt.Printf("Scanning directories for files...")
	refFileDescriptions := ScanDirForFileInfo(refDirs.S)
	searchFileDescriptions := FileDescSlice{}
	if *referenceDirectoryPtr != *searchDirectoryPtr {
		searchFileDescriptions = ScanDirForFileInfo(searchDirs.S)
	} else {
		searchFileDescriptions.S = refFileDescriptions.S
	}
	fmt.Printf("\n\tFound %d reference files to compare against %d search files.\n", len(refFileDescriptions.S), len(searchFileDescriptions.S))
	if *debugModePtr {
		fmt.Printf("refFileDescriptions (%d):\n%s\n\n", len(refFileDescriptions.S), refFileDescriptions.S)
	}
	if *debugModePtr {
		fmt.Printf("searchFileDescriptions (%d):\n%s\n\n", len(searchFileDescriptions.S), searchFileDescriptions.S)
	}

	// Find the matches, returns a []PairedFiles.
	fmt.Printf("Searching for files with the same size...")
	compareFields := CompareFields{UseName: *useNamePtr, UseSize: *useSizePtr, UseModDate: *useModDatePtr, UseChecksum: false}
	fileDescMatches := FindMatchingFileDescriptions(&refFileDescriptions.S, &searchFileDescriptions.S, compareFields)
	fmt.Printf("\n\tStage 1 search found %d potential duplicate(s).\n", len(*fileDescMatches))
	if *debugModePtr {
		fmt.Printf("fileDescMatches (%d):\n%s\n\n", len(*fileDescMatches), *fileDescMatches)
	}

	// Create a slice of file paths to checksum. []string
	fmt.Printf("Building a list of files that need hashes...")
	filesToChecksum := FilesToChecksum(fileDescMatches)
	fmt.Printf("\n\tFound %d files that need checksums calculated.\n", len(*filesToChecksum))
	if *debugModePtr {
		fmt.Printf("filesToChecksum (%d):\n%s\n\n", len(*filesToChecksum), *filesToChecksum)
	}

	// Get the checksums for the necessary files. map[string]string map[fileAbsPath]fileHash
	fmt.Printf("Calculating hashes for files that may be duplicates...")
	fileChecksums := ChecksumFiles(filesToChecksum)
	fmt.Printf("\n\tDone.\n")
	if *debugModePtr {
		fmt.Printf("fileChecksums (%d):\n%s\n\n", len(fileChecksums), fileChecksums)
	}

	// Make a new []PairedFiles with checksums filled in (not checked yet).
	fmt.Printf("Calculating matches...")
	fileDescMatchesWithChecksums := AddChecksumsToMatched(fileChecksums, fileDescMatches)
	fmt.Printf("\n\tAlmost done...\n")
	if *debugModePtr {
		fmt.Printf("fileDescMatchesWithChecksums (%d):\n%s\n\n", len(*fileDescMatchesWithChecksums), *fileDescMatchesWithChecksums)
	}

	// Get a []PairedFiles with only matching files.
	fmt.Printf("\tStill calculating...")
	fileChecksumMatches := FindMatchingFileChecksums(fileDescMatchesWithChecksums)
	fmt.Printf("\n\tDone.\n")
	if *debugModePtr {
		fmt.Printf("fileChecksumMatches (%d):\n%s\n\n", len(*fileChecksumMatches), *fileChecksumMatches)
	}

	// Map the files from the search dir that match each file from the ref dir. (map[refFileDesc][]searchFileDesc)
	fmt.Printf("Mapping duplicate files...")
	mappedChecksumMatches := MapChecksumMatches(fileChecksumMatches)
	// Add up the total number of duplicate files found in the search dir(s).
	dupeCount := 0
	for _, checksumMatches := range mappedChecksumMatches.M {
		dupeCount += len(checksumMatches)
	}
	fmt.Printf("\n\tFound %d files with %d duplicates.\n", len(mappedChecksumMatches.M), dupeCount)
	if *debugModePtr {
		for refFileDesc := range mappedChecksumMatches.M {
			fmt.Printf("%s:\n%s\n\n", refFileDesc.AbsPath, mappedChecksumMatches.M[refFileDesc])
		}
	}

	// If no matches were found just say so and bail.
	if len(mappedChecksumMatches.M) == 0 {
		fmt.Printf("\nNo duplicates found.\n")
		os.Exit(0)
	}

	// List the duplicates.
	if *listDupesPtr || *deleteDupesPtr {
		ListDupes(mappedChecksumMatches.M)
	}

	// Delete the duplicates, if asked to.
	if *deleteDupesPtr {
		fmt.Printf("Are you sure you would like to delete the above files (type YES)? ")
		var input string
		_, err := fmt.Scan(&input) // Accept user verification.
		checkError(err)
		// Only delete files if the user typed "YES" in all caps.
		if input == "YES" {
			DeleteDupes(mappedChecksumMatches.M)
		}
		fmt.Printf("\nDone.\n")
		os.Exit(0)
	}
	// Fall-through exit.
	os.Exit(0)
}
