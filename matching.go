// Matching contains functions to use for detecting matches.
package main

import (
	"sync"
)

// CompareFields stores settings for camparing files.
type CompareFields struct {
	UseName     bool
	UseSize     bool
	UseModDate  bool
	UseChecksum bool
}

// PairedFiles is used for storing info about a (potential) ref/search match.
type PairedFiles struct {
	RefFileDesc    FileDescription
	SearchFileDesc FileDescription
}

// CompareFileInfoJob stores everything needed for CompareFileInfoWorker.
type CompareFileInfoJob struct {
	RefFileDesc    FileDescription
	SearchFileDesc FileDescription
	Fields         CompareFields
}

// MappedChecksumMatches is a map for storing the 2nd stage matches.
// Use mappedChecksumMatches.lock() and .unlock() for mutex locking.
type MappedChecksumMatches struct {
	M map[FileDescription][]FileDescription
	sync.Mutex
}

// CompareFileInfoWorker compares two FileDescriptions, sends a PairedFiles with both FileDescriptions if things match.
func compareFileInfoWorker(compareJob *CompareFileInfoJob, matchResultsChan chan PairedFiles) {
	// First make sure we're not comparing a file with itself.
	if compareJob.RefFileDesc.AbsPath == compareJob.SearchFileDesc.AbsPath {
		return
	}
	if compareJob.Fields.UseName {
		if !(compareJob.RefFileDesc.FileInfo.Name() == compareJob.SearchFileDesc.FileInfo.Name()) {
			return
		}
	}
	if compareJob.Fields.UseSize {
		if !(compareJob.RefFileDesc.FileInfo.Size() == compareJob.SearchFileDesc.FileInfo.Size()) {
			return
		}
	}
	if compareJob.Fields.UseModDate {
		if !(compareJob.RefFileDesc.FileInfo.ModTime() == compareJob.SearchFileDesc.FileInfo.ModTime()) {
			return
		}
	}
	if compareJob.Fields.UseChecksum {
		if !(compareJob.RefFileDesc.Checksum == compareJob.SearchFileDesc.Checksum) {
			return
		}
	}
	// If everything matched send the PairedFiles over the channel.
	matchResultsChan <- PairedFiles{RefFileDesc: compareJob.RefFileDesc, SearchFileDesc: compareJob.SearchFileDesc}
}

// FindMatchingFileDescriptions returns a []PairedFiles (with matching FileDescriptions).
func FindMatchingFileDescriptions(refFileDescriptions *[]FileDescription, searchFileDescriptions *[]FileDescription, compareFields CompareFields) *[]PairedFiles {
	comboCount := len(*refFileDescriptions) * len(*searchFileDescriptions)
	// Set up channels for workers.
	matchResultsChan := make(chan PairedFiles, comboCount)
	// Set up a WaitGroup to sync things up.
	workerWaitGroup := &sync.WaitGroup{}
	workerWaitGroup.Add(comboCount)

	// Iterate through the reference files in the outer loop.
	for _, refFileDescription := range *refFileDescriptions {
		// Iterate through the search files in the inner loop.
		for _, searchFileDescription := range *searchFileDescriptions {
			// Give the workers their jobs.
			go func(newCompareJob *CompareFileInfoJob, matchResultsChan chan PairedFiles) {
				defer workerWaitGroup.Done()
				compareFileInfoWorker(newCompareJob, matchResultsChan)
			}(&CompareFileInfoJob{RefFileDesc: refFileDescription, SearchFileDesc: searchFileDescription, Fields: compareFields}, matchResultsChan)
		}
	}
	workerWaitGroup.Wait()
	close(matchResultsChan)
	var fileDescriptionMatches []PairedFiles
	for i := 1; i <= comboCount; i++ {
		matchResult := <-matchResultsChan
		nilFileDescription := FileDescription{}
		if (matchResult.RefFileDesc != nilFileDescription) && (matchResult.SearchFileDesc != nilFileDescription) {
			// Only add non-nil results. (this was for demo purposes, but shouldn't hurt anything)
			fileDescriptionMatches = append(fileDescriptionMatches, matchResult)
		}
	}
	return &fileDescriptionMatches
}

// FindMatchingFileChecksums returns a []PairedFiles with only matching files.
// This is most of the 2nd stage search.
func FindMatchingFileChecksums(fileDescMatchesWithChecksums *[]PairedFiles) *[]PairedFiles {
	checksumResultChan := make(chan PairedFiles, len(*fileDescMatchesWithChecksums))
	workerWaitGroup := &sync.WaitGroup{}
	workerWaitGroup.Add(len(*fileDescMatchesWithChecksums))

	// Iterate over each pair of files that matched in the 1st stage search.
	for _, matchedFiles := range *fileDescMatchesWithChecksums {
		// Spin up workers to test whether the checksums of the two files match.
		go func(checksumResultChan chan PairedFiles, matchedFiles PairedFiles) {
			defer workerWaitGroup.Done()
			if matchedFiles.RefFileDesc.Checksum == matchedFiles.SearchFileDesc.Checksum {
				// Checksums matched.
				checksumResultChan <- matchedFiles
			}
		}(checksumResultChan, matchedFiles)
	}

	workerWaitGroup.Wait()
	close(checksumResultChan)

	fileChecksumMatches := make([]PairedFiles, 0)
	nilFileDesc := FileDescription{}
	for i := 1; i <= len(*fileDescMatchesWithChecksums); i++ {
		matchedFiles := <-checksumResultChan
		if matchedFiles.RefFileDesc == nilFileDesc ||
			matchedFiles.SearchFileDesc == nilFileDesc {
			continue // Skip nil FileDescriptions
		}
		fileChecksumMatches = append(fileChecksumMatches, matchedFiles)
	}
	return &fileChecksumMatches
}

// mapChecksumMatch is the worker for mapChecksumMatches.
func mapChecksumMatch(jobsChan <-chan PairedFiles, mappedChecksumMatches *MappedChecksumMatches, wg *sync.WaitGroup) {
	for job := range jobsChan {
		// Don't map files to themselves.
		if job.RefFileDesc.AbsPath == job.SearchFileDesc.AbsPath {
			wg.Done()
			continue
		}
		// Don't accept empty checksums.
		if job.RefFileDesc.Checksum == "" ||
			job.SearchFileDesc.Checksum == "" {
			wg.Done()
			continue
		}
		// Compare the checksums of the files.
		if job.RefFileDesc.Checksum == job.SearchFileDesc.Checksum {
			// The files match, map them.
			matchingPair := PairedFiles{RefFileDesc: job.RefFileDesc, SearchFileDesc: job.SearchFileDesc}
			// Mutex lock mappedChecksumMatches until we're done touching it.
			mappedChecksumMatches.Lock()
			if _, exists := mappedChecksumMatches.M[matchingPair.RefFileDesc]; exists {
				// it was in the map.
				mappedChecksumMatches.M[matchingPair.RefFileDesc] = append(mappedChecksumMatches.M[matchingPair.RefFileDesc], matchingPair.SearchFileDesc)
			} else {
				// it wasn't in the map, init it first.
				mappedChecksumMatches.M[matchingPair.RefFileDesc] = make([]FileDescription, 0)
				mappedChecksumMatches.M[matchingPair.RefFileDesc] = append(mappedChecksumMatches.M[matchingPair.RefFileDesc], matchingPair.SearchFileDesc)
			}
			mappedChecksumMatches.Unlock()
		}
		wg.Done()
	}
}

// MapChecksumMatches maps the files from the search dir that match each file from the ref dir. (map[refFileDesc][]searchFileDesc)
func MapChecksumMatches(fileChecksumMatches *[]PairedFiles) MappedChecksumMatches {
	// create a slice for refFileDescriptions and a slice for searchFileDescriptions
	var refFileDescs []FileDescription
	var searchFileDescs []FileDescription
	for _, matchedFiles := range *fileChecksumMatches {
		// Add the refFiles, then the searchFiles.
		if !FileDescInSlice(matchedFiles.RefFileDesc, refFileDescs) {
			refFileDescs = append(refFileDescs, matchedFiles.RefFileDesc)
		}
		if !FileDescInSlice(matchedFiles.SearchFileDesc, searchFileDescs) {
			searchFileDescs = append(searchFileDescs, matchedFiles.SearchFileDesc)
		}
	}
	mappedChecksumMatches := MappedChecksumMatches{M: make(map[FileDescription][]FileDescription)}
	comboCount := len(refFileDescs) * len(searchFileDescs)
	wg := &sync.WaitGroup{}
	wg.Add(comboCount)
	jobsChan := make(chan PairedFiles, comboCount)
	workerCount := 512
	if comboCount < workerCount {
		workerCount = comboCount
	}

	for i := 1; i <= workerCount; i++ {
		// Spin up many workers to process the matching pairs of FileDescriptions.
		go mapChecksumMatch(jobsChan, &mappedChecksumMatches, wg) // (workers are blocked until fed by jobsChan)
	}

	// Iterate over the ref files in outer loop.
	for _, refFileDesc := range refFileDescs {
		// Then iterate over the search files in the inner loop.
		for _, searchFileDesc := range searchFileDescs {
			// Assemble a job and send it to a worker.
			jobsChan <- PairedFiles{RefFileDesc: refFileDesc, SearchFileDesc: searchFileDesc}
		}
	}

	// Now wait for all of the results to be processed.
	wg.Wait()
	return MappedChecksumMatches{M: mappedChecksumMatches.M}
}
