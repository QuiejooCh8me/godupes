// tests for fuctions in otherutils.go

package main

import (
	"os"
	"testing"
)

func TestStringInSlice(t *testing.T) {
	testSlice := make([]string, 3)
	testSlice[0] = "dog"
	testSlice[1] = "cat"
	testSlice[2] = "emu"
	if !StringInSlice("cat", testSlice) ||
		!StringInSlice("dog", testSlice) ||
		!StringInSlice("emu", testSlice) {
		t.Errorf("StringInSlice did not return true when it should have.")
	}
	if StringInSlice("3244234", testSlice) ||
		StringInSlice("4324ui4h14", testSlice) ||
		StringInSlice("39Gzl8Z`r WOg-OrSR80 x76LwUu/*1", testSlice) {
		t.Errorf("StringInSlice did not return false when it should have.")
	}
}

func TestFileDescInSlice(t *testing.T) {
	testSlice := make([]FileDescription, 1)
	testFileInfo, err := os.Stat("/home")
	checkError(err)
	testFileDescription := FileDescription{AbsPath: "test1", FileInfo: testFileInfo}
	badFileInfo, err := os.Stat("/usr")
	checkError(err)
	badFileDescription := FileDescription{AbsPath: "test2", FileInfo: badFileInfo}
	checkError(err)
	testSlice[0] = testFileDescription
	if !FileDescInSlice(testFileDescription, testSlice) {
		t.Errorf("FileDescInSlice did not return true when it should have.")
	}
	if FileDescInSlice(badFileDescription, testSlice) {
		t.Errorf("FileDescInSlice did not return false when it should have.")
	}
}
